file = open("Resume.txt", "r+")     #Resume.txt is the file you want to remove Plagiarism from
for word in file.read():
    change = ""
    if word in "aeiou":
        change += "hi"     #'hi' is the word you want it to change from i.e the vowel
    else:
        change += word
    file.write(change)
file.close()
